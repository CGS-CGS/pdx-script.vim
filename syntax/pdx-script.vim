" Syntax
" Keywords

" Literals
" Boolean
syn keyword pdxBoolean yes no
" Number
syn match pdxNumber display "\v<\d+>"

" Misc
" Comments
syn region pdxComment start="#", end="$" contains=@Spell

" Default highlighting
hi def link pdxBoolean Boolean
hi def link pdxNumber Number
hi def link pdxComment Comment

" vim: set et sw=4 sts=4 ts=8:
